﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;


public class Scorekeeper : MonoBehaviour {
    static private Scorekeeper instance;
    public int pointsPerGoal = 1;
    private int[] score = new int[2];
    public Text[] scoreText;
    public bool startTimer = false;
    public int endtimer = 60;
    public int winCond = 10;
    // Use this for initialization
    void Start () {
        for (int i = 0; i < score.Length; i++)
        {
            score[i] = 0;
            scoreText[i].text = "0";
        }

        if (instance == null)
        {
            // save this instance
            instance = this;
        }
        else
        {
            // more than one instance exists
            Debug.LogError(
                "More than one Scorekeeper exists in the scene.");
        }
    }

// Update is called once per frame
void Update () {
		if (startTimer == true)
        {
            endtimer -= 1;
        }
        if (endtimer <= 0)
        {
            UnityEditor.EditorApplication.isPlaying = false;
        }
	}
public void OnScoreGoal(int player)
    {
        score[player] += pointsPerGoal;
        scoreText[player].text = score[player].ToString();
        if (score[player] >= winCond)
        {
            scoreText[score.Length].text = "Player" + (player + 1) + " " + "Wins!";
            startTimer = true;
        }
    }
    static public Scorekeeper Instance
    {
        get { return instance; }
    }

}
