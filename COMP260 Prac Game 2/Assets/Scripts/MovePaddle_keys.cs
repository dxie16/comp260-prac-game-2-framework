﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePaddle_keys : MonoBehaviour {
    private new Rigidbody rigidbody;
    public float speed = 5.0f; // in metres per second
    public Vector3 move;
    public Vector3 velocity; // in metres per second
    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("Time = " + Time.time);
    }
    void FixedUpdate()
    {
        float forwards = Input.GetAxis("Vertical");
        float left = Input.GetAxis("Horizontal");
        // the horizontal axis controls the turn
        Vector3 dir = (left*Vector3.right + forwards*Vector3.forward)*speed;
        Vector3 vel = dir.normalized * speed;
        // check is this speed is going to overshoot the target
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;
        if (move > distToTarget)
        {
            // scale the velocity down appropriately
            vel = vel * distToTarget / move;
        }
        rigidbody.velocity = vel;
    }

}
