﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class EPaddle : MonoBehaviour {

    private new Rigidbody rigidbody;
    public float force = 5f;
    public float speed = 10f;
    public Transform GoalPos;
    public Transform CenterPos;
    public Transform PuckPos;
    public Transform DeadPos;
    Vector3 pos;
    // Use this for initialization
    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }

    // Update is called once per frame
    void Update()
    {
    }
    void FixedUpdate()
    {
        float locatePuckx = PuckPos.transform.position.x;
        float locatePuckZ = PuckPos.transform.position.z;
        float locateCent = CenterPos.transform.position.x;
        float locateDPX = DeadPos.transform.position.x;
        float locateDPZ = DeadPos.transform.position.z;
        if (locatePuckx >= locateDPX && Mathf.Abs(locatePuckZ) >= Mathf.Abs(locateDPZ))
        {
            pos = GoalPos.position;
        }
        else
        {
            if (locateCent <= locatePuckx)
                pos = PuckPos.position;
            else
                pos = GoalPos.position;

        }
        Vector3 dir = pos - rigidbody.position;
        Vector3 vel = dir.normalized * speed;

        // check is this speed is going to overshoot the target
        float move = speed * Time.fixedDeltaTime;
        float distToTarget = dir.magnitude;

        if (move > distToTarget)
        {
            // scale the velocity down appropriately
            vel = vel * distToTarget / move;
        }

        rigidbody.velocity = vel;
        rigidbody.AddForce(dir.normalized * force);
    }
    void OnTriggerEnter(Collider collider)
    {
        pos = CenterPos.transform.position;
    }

}
