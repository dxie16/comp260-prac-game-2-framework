﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePaddle_Trans : MonoBehaviour {
    private new Rigidbody rigidbody;
    public float force = 10f;
    public float speed = 20f;
    private Vector3 GetMousePosition()
    {
        // create a ray from the camera 
        // passing through the mouse position
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        // find out where the ray intersects the XZ plane
        Plane plane = new Plane(Vector3.up, Vector3.zero);
        float distance = 0;
        plane.Raycast(ray, out distance);
        return ray.GetPoint(distance);
    }
    // Use this for initialization
    void Start () {
        rigidbody = GetComponent<Rigidbody>();
        rigidbody.useGravity = false;
    }
	
	// Update is called once per frame
	void Update () {
        Debug.Log("Time = " +Time.time);
    }
    void FixedUpdate()
    {
        Vector3 pos = GetMousePosition();
        rigidbody.position = pos;
    }

    void OnDrawGizmos()
    {
        // draw the mouse ray
        Gizmos.color = Color.yellow;
        Vector3 pos = GetMousePosition();
        Gizmos.DrawLine(Camera.main.transform.position, pos);
    }
}
